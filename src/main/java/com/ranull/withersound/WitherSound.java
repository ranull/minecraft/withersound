package com.ranull.withersound;

import com.ranull.withersound.listener.EntityExplodeListener;
import com.ranull.withersound.listener.WorldEventListener;
import com.ranull.withersound.command.WitherSoundCommand;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

public final class WitherSound extends JavaPlugin {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        registerCommands();
        registerListeners();
    }

    @Override
    public void onDisable() {
        unregisterListeners();
    }

    private void registerCommands() {
        PluginCommand witherSoundPluginCommand = getCommand("withersound");

        if (witherSoundPluginCommand != null) {
            WitherSoundCommand witherSoundCommand = new WitherSoundCommand(this);

            witherSoundPluginCommand.setExecutor(witherSoundCommand);
            witherSoundPluginCommand.setTabCompleter(witherSoundCommand);
        }
    }

    private void registerListeners() {
        new WorldEventListener(this);
        getServer().getPluginManager().registerEvents(new EntityExplodeListener(this), this);
    }

    private void unregisterListeners() {
        HandlerList.unregisterAll(this);
    }
}
